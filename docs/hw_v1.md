# Komponenten


## Günstige Variante

| Name | Kurzbezeichnung | Beschreibung | Preis |
| ---- | --------------- | ------------ | ----- |
| CPU  | AMD Athlon 200GE | APU, AM4, 2 Core | 30 |
| CPU-Lüfter | Thermalright AXP90-X36 Black | Low-Profile | 20 |
| Mainboard | Gigabyte B550I Aorus Pro AX | Mini ITX, AM4, AMD B550 Chipset | 160 |
| RAM | G.Skill NT Series | 2 x 8GB, DDR4 | 25 |
| Netzteil | Akyga Pro 300W SFX |  | 25 |
| Gehäuse | SilverStone Milo ML05 |  | 50 |
| Gehäuse Filter | Intake Filter |  | 25 |
| Gehäuse-Lüfter | Noctua NF-A9x14 PWM | Platziert unter dem Mainboard | 19 |
| SSD-Lüfter | Noctua NF-A12x25 PWM | Platziert als SSD Kühlung | 15 |
| SSD SATA Kabel | Datenkabel | mit Klip und nicht rechtwinklig | (4x2) 8 |
| SSD Mount | SSD-Gestell 3D-Druck | Im Gehäuse auf der Seite wo normalerweise die GPU ist, hat es platz für ein 2x2 internes SSD-Gestell | 10 |
| Front Panel Adapter | Alt zu Modern Adapter | USB-C (Typ-E 20-Pin Key A) zu USB Header (Interner 19-Pin Stecker) | 5 |
|  |  |  |  |

---



Preise in CHF.

| Name | Kurzbezeichnung | Beschreibung | Preis |
| ---- | --------------- | ------------ | ----- |
| CPU  | AMD Ryzen 4300G | APU, AM4, 3.80 GHz, 4 Core | 80 |
| CPU-Lüfter | Noctua NH-L9a-AM4 | Low-Profile | 45 |
| Mainboard | Gigabyte B550I Aorus Pro AX | Mini ITX, AM4, AMD B550 Chipset | 160 |
| RAM | Team Group T-Force Vulcan Z | 2 x 8GB, 3200 MHz, DDR4, TLZRD416G3200HC16CDC01 | 30 |
| Netzteil | Sharkoon Netzteil SilentStorm SFX Gold 500 W | 200-300W wären ausreichend, muss vier mal SATA-Strom liefern | 90 |
| Gehäuse | Dan Cases A4-SFX V4.1 | SFF, ITX, Sandwich | 190 |
| Gehäuse Filter | Intake Filter |  | 25 |
| Gehäuse-Lüfter | Noctua NF-A9x14 PWM | Platziert unter dem Mainboard | 19 |
| SSD-Lüfter | Noctua NF-A12x25 PWM | Platziert als SSD Kühlung | 15 |
| SSD SATA Kabel | Datenkabel | mit Klip und nicht rechtwinklig | (4x2) 8 |
| SSD Mount | SSD-Gestell 3D-Druck | Im Gehäuse auf der Seite wo normalerweise die GPU ist, hat es platz für ein 2x2 internes SSD-Gestell | 10 |
| Front Panel Adapter | Alt zu Modern Adapter | USB-C (Typ-E 20-Pin Key A) zu USB Header (Interner 19-Pin Stecker) | 5 |
|  |  |  |  |


Wichtig!
- SSDs mit kleinerer Kapazität sind oftmals langsamer als grössere.
- SSDs sowie auch HDDs sollten Zeit versetzt gekauft werden. Somit werden Produktionsfehler vermieden.


#### OS SSD Optionen

| Name | Kurzbezeichnung | Beschreibung | Preis |
| ---- | --------------- | ------------ | ----- |
| SSD | Kioxia Exceria G2 | Wird zweimal benötigt! 500 GB, TBW: 200 TB, M.2 2280, LRC20Z500GG8 | (50x2) 100 |
| SSD | WD Black SN770 | Wird zweimal benötigt! 500 GB, TBW: 300 TB, M.2 2280, WDS500G3X0E-00B3N0 | (50x2) 100 |

### Speichermedium Optionen

Es werden mindestens vier Disks benötigt!
Je nach Risikobereitschaft wird die Hälfte der Disks für Parity benötigt.
Somit steht nur die Hälfte der totalen Kapazität zur Verfügung.
Es kann aber mit einem bis zu doppelten Anstieg an Lese-Geschwindigkeit gerechnet werden.  


#### SSD

| Name | Kurzbezeichnung | Beschreibung | Grösse | Preis |
| ---- | --------------- | ------------ | ------ | ----- |
| Crucial Mx500 | CT1000MX500SSD1 | TBW: 360 TB, RW: 510 MB/s, Random 4k R/W: 90000 IOPS, NAND: TLC | 1TB | 80 |
|  |  |  |  |


#### HDD

| Name | Kurzbezeichnung | Grösse | Preis |
| ---- | --------------- | ------ | ----- |
| Western Digital WD Blue | WD10EZRZ | 1TB | 40 |
| Seagate BarraCuda | ST31000528AS | 1TB | 40 |
| Toshiba Enterprise Capacity 24/7 | MG04ACA200E | 2TB | 50 |

|  |  |  |  |




## Totale Kosten

Totale Hardware Kosten der Version 1 ohne Speichermediume: 777.-
Günstige Variante: 468.-


Zuzüglich der 4 Benötigten Speichermedium à 1TB: 1097.-
Günstige Variante: 788.-
Günstige Variante mit HDDs und doppelter Kapazität: 668.-

---


## Bauanleitung

- Gehäuse komplett auseinander bauen.
- Gehäuse-Lüfter unter dem Mainboard-Platz installieren.
- CPU, RAM und OS-SSDs installieren.
- CPU-Lüfter installieren.

