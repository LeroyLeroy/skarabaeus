# Skarabaeus

Ganzheitliches Backup System. Batteries included.  
Von Hardware über Software bis hin zu Offsite Backup.  
~Single Board Computer (SBC) mit PCIe-Schnittstelle für diverse Speichermedien.~  
x86_64 CPU für Kompatibilität mit Software und einfacher Erweiterung- und Erneuerungsmöglichkeiten.  
Optimaler Einsatz mit mindestens vier Disks für ZFS RAIDz2.  
Gesteuert über ein Web-Interface.  
Ermöglicht das hochladen von Daten und das synchronisieren von Ordner über mehrere Geräte.  
Automatisierte Backups und durchsuchbare Snapshots für die Wiederherstellung.  


## Aufbauend auf

- restic
- ZFS
- NixOS
- Syncthing
- rsync.net
- WireGuard


## Hardware muss mindestens aus den folgenden Elementen bestehend

- CPU (beforzugterweise APU)
- CPU Lüfter
- Mainboard
- RAM
- Netzteil
- Netzwerkkabel
- 2x Betriebssystem Speichermedium (beforzugterweise NVMe M.2 SSD)
- 4x Speichermedien (falls über SATA angeschlossen mit Kabel)
- Gehäuse (mit Filtersystem für die Frischluft)
- Gehäuselüfter

[Auflistung der Komponenten der Test-Version 1](./docs/hw_v1.md)


## Verwendete Technologien

- restic
- SQLite
- SyncThing
- NixOS
- Tailscale, NetBird, WireGuard, ZeroTier
- Python
    - Flask
    - flask-sqlalchemy -> wird mit "SQLModel" ersetzt
    - flask-migrate
    - FastAPI
    - Redis -> wird mit "valkey" oder "keydb" ersetzt, sobald diese stable sind
    - rq
    - rqscheduler
    - resticpy
    - pyncthing
    - xkcdpass
    - flask-qrcode
    - pyotp
