# Skarabäus API written in [FastAPI](https://fastapi.tiangolo.com/)

### Dev

Install the following packages
```shell
git
python311
poetry
python311Packages.pydevd
jetbrains.pycharm-professional
```


### First time setup

```shell
poetry init
poetry install
```