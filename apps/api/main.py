from typing import Annotated
from datetime import timedelta

from fastapi.security import OAuth2PasswordRequestForm
from fastapi import Depends, FastAPI, HTTPException, status
from sqlalchemy.orm import Session

from src import crud, schemas, config

app = FastAPI()


@app.get("/info")
async def info(settings: Annotated[config.Settings, Depends(crud.get_settings)]):
    return {
        "app_name": settings.app_name,
        "admin_email": settings.admin_email,
    }


@app.get("/needed_packages")
async def needed_packages():
    packages = [{
        'name': 'restic',
        'version': '0.16.4',
        'hash': '6n0hfv7klm0zkidc6nd1h5k1mq1cikzi',
        'validated': False
    }]
    return packages


@app.get("/os_config")
async def os_config():
    return 'os config'


@app.post("/user/add", response_model=schemas.User)
async def create_user(
        form_data: Annotated[schemas.UserBase, Depends()],
        db: Session = Depends(crud.get_db)
):
    has_created_user, user = crud.create_user(db, user=form_data)
    if has_created_user:
        return user
    else:
        raise HTTPException(
            status_code=status.HTTP_406_NOT_ACCEPTABLE,
            detail="user already exists",
        )


@app.get("/user/", response_model=schemas.User)
async def read_user(
        current_user: Annotated[schemas.User, Depends(crud.get_current_active_user)],
):
    return current_user


@app.post("/user/token")
async def login_for_access_token(
        form_data: Annotated[OAuth2PasswordRequestForm, Depends()],
        settings: Annotated[config.Settings, Depends(crud.get_settings)],
        db: Session = Depends(crud.get_db)
) -> schemas.Token:
    user = crud.authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=settings.access_token_expire_minutes)
    access_token = crud.create_access_token(
        settings=settings,
        data={"sub": user.email},
        expires_delta=access_token_expires
    )
    return schemas.Token(access_token=access_token, token_type="bearer")


@app.post("/user/confirm", response_model=schemas.User)
async def confirm_user(
        current_user: Annotated[schemas.User, Depends(crud.get_current_active_user)],
        form_data: Annotated[schemas.UserConfirmation, Depends()],
        db: Session = Depends(crud.get_db)
):
    return crud.confirm_user(db, email=form_data.email, user=current_user)


@app.post("/user/add_device", response_model=schemas.Device)
async def user_add_device(
        current_user: Annotated[schemas.User, Depends(crud.get_current_active_user)],
        form_data: Annotated[schemas.DeviceBase, Depends()],
        db: Session = Depends(crud.get_db)
):
    return crud.create_device(db, device=form_data, user=current_user)
