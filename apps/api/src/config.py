from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    app_name: str = "Skarabäus API"
    admin_email: str = "spam@me.com"
    access_token_expire_minutes: int
    secret_key: str
    algorithm: str

    model_config = SettingsConfigDict(env_file=".env", env_file_encoding='utf-8')
