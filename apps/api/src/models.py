from sqlalchemy import Boolean, ForeignKey, Integer, Float, String, DateTime, func
from sqlalchemy.orm import relationship, mapped_column

from .database import Base


class User(Base):
    __tablename__ = "users"

    id = mapped_column(Integer, primary_key=True)
    email = mapped_column(String, unique=True, index=True)
    password = mapped_column(String)
    is_active = mapped_column(Boolean, default=True)
    confirmed = mapped_column(Boolean, default=False)
    created_at = mapped_column(DateTime, default=func.now())

    devices = relationship("Device", back_populates="owner")


class Device(Base):
    __tablename__ = "devices"

    id = mapped_column(Integer, primary_key=True)
    device_version = mapped_column(Float)
    os_version = mapped_column(Float)
    created_at = mapped_column(DateTime, default=func.now())
    owner_id = mapped_column(Integer, ForeignKey("users.id"))

    owner = relationship("User", back_populates="devices")


class Changelog(Base):
    __tablename__ = "changelogs"

    id = mapped_column(Integer, primary_key=True)
    table = mapped_column(String, index=True)
    changelog = mapped_column(String, index=True)
    inserted_at = mapped_column(DateTime, default=func.now())
