from pydantic import BaseModel


class DeviceBase(BaseModel):
    device_version: float
    os_version: float


class DeviceCreate(DeviceBase):
    pass


class Device(DeviceBase):
    id: int
    owner_id: int

    class Config:
        from_attributes = True


class UserBase(BaseModel):
    email: str
    password: str


class UserConfirmation(BaseModel):
    email: str


class User(UserBase):
    id: int
    is_active: bool
    confirmed: bool
    devices: list[Device] = []

    class Config:
        from_attributes = True


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: str | None = None
