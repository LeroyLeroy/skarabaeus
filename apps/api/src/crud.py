from datetime import timedelta, timezone, datetime
from functools import lru_cache
from typing import Annotated

from passlib.context import CryptContext
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import jwt, JWTError
from sqlalchemy.orm import Session

from . import models, schemas, database, config


models.Base.metadata.create_all(bind=database.engine)
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/user/token")


@lru_cache
def get_settings():
    return config.Settings()


def get_db():
    db = database.SessionLocal()
    try:
        yield db
    finally:
        db.close()


def create_access_token(
        settings: Annotated[config.Settings, Depends(get_settings)],
        data: dict,
        expires_delta: timedelta | None = None,
):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.now(timezone.utc) + expires_delta
    else:
        expire = datetime.now(timezone.utc) + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, settings.secret_key, algorithm=settings.algorithm)
    return encoded_jwt


def get_user_by_email(db: Session, email: str):
    return db.query(models.User).filter(models.User.email == email).first()


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def create_user(db: Session, user: schemas.UserBase):
    existing_user = get_user_by_email(db, user.email)
    if existing_user:
        return False, existing_user

    db_user = models.User(
        email=user.email,
        password=get_password_hash(user.password),
        is_active=True
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return True, db_user


def confirm_user(db: Session, email: str, user: schemas.User):
    if not user.is_active:
        raise HTTPException(status_code=400, detail="Inactive user")
    db_user = get_user_by_email(db, email)
    db_user.confirmed = True
    db.commit()
    db.refresh(db_user)
    return db_user


def create_device(db: Session, device: schemas.DeviceBase, user: schemas.User):
    db_device = models.Device(
        device_version=device.device_version,
        os_version=device.device_version,
        owner_id=user.id
    )
    db.add(db_device)
    db.commit()
    db.refresh(db_device)
    return db_device


async def get_current_user(
        token: Annotated[str, Depends(oauth2_scheme)],
        settings: Annotated[config.Settings, Depends(get_settings)],
        db: Session = Depends(get_db)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, settings.secret_key, algorithms=[settings.algorithm])
        email: str = payload.get("sub")
        if email is None:
            raise credentials_exception
        token_data = schemas.TokenData(email=email)
    except JWTError:
        raise credentials_exception
    user = get_user_by_email(db, email=token_data.email)
    if user is None:
        raise credentials_exception
    return user


async def get_current_active_user(
        current_user: Annotated[schemas.User, Depends(get_current_user)],
):
    if not current_user.is_active:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user


def authenticate_user(db, email: str, password: str) -> any:
    db_user = get_user_by_email(db, email)
    if not db_user:
        return False
    if not verify_password(password, db_user.password):
        return False
    return db_user
