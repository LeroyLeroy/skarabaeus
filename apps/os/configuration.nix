{ config, lib, pkgs, ... }:
let
  user_name = "leroy";
  restic_repo_path = "/mnt/data/skarabaeus/restic";
in
{
  imports =
    [
      ./hardware-configuration.nix
    ];

  # Use the extlinux boot loader. (NixOS wants to enable GRUB by default)
  boot.loader.grub.enable = false;
  # Enables the generation of /boot/extlinux/extlinux.conf
  boot.loader.generic-extlinux-compatible.enable = true;
  boot.kernelPackages = pkgs.linuxPackages_6_6;

  boot.supportedFilesystems = [ "zfs" ];
  boot.zfs.extraPools = [ "bigdata" ];
  boot.zfs.forceImportRoot = false;
  # boot.kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;

  networking.hostId = "409f596f";
  networking.hostName = "skarabaeus0"; # Define your hostname.
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  networking = {
    dhcpcd.enable = false;
    interfaces.end0.ipv4.addresses = [{
      address = "192.168.1.20";
      prefixLength = 24;
    }];
    defaultGateway = "192.168.1.1";
    nameservers = ["192.168.1.1"];
  };


  # Set your time zone.
  time.timeZone = "Europe/Zurich";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "sg";
    # useXkbConfig = true; # use xkb.options in tty.
  };

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  security = {
    sudo = {
      enable = true;
      wheelNeedsPassword = false;
    };
  };

  users.users."${user_name}" = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" ]; # Enable ‘sudo’ for the user.
    packages = with pkgs; [
      wget
      git
    ];
  };

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  services.zfs.autoScrub.enable = true;

  nix.settings = {
    # Deduplicate and optimize nix store
    auto-optimise-store = true;
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    zfs
    python311
    poetry
    ethtool
    restic
  ];

  # syncthing
  services = {
    syncthing = {
      enable = true;
      user = "${user_name}";
      openDefaultPorts = true;
      guiAddress = "0.0.0.0:8384";
      dataDir = "/mnt/data/skarabaeus/sync";  # default folder for new synced folders, unused for now
      configDir = "/home/${user_name}/.config/syncthing";
      overrideDevices = false;  # allows to add new devices
      overrideFolders = false;  # for now only one folder, the default one
      settings = {
        gui = {
          apiKey = "skarabaeus_testing";  # change this in prod, and save the key for use in skarabaeus
        };

        folders = {
          "skarabaeus" = {  # Name of folder in Syncthing, also the folder ID
            path = "/mnt/data/skarabaeus/sync";  # folder location on the fs
            ignorePerms = true;
          };
        };
      };
    };
    restic.backups = {
      localbackup = {
        initialize = true;
        user = "${user_name}";
        passwordFile = "/home/${user_name}/skarabaeus/apps/gui/password.txt";
        repository = "${restic_repo_path}";
        timerConfig = {
          OnCalendar = "daily";
          Persistent = true;
        };
        paths = [
          "/mnt/data/skarabaeus/upload"
        ];
      };
    };
    redis.servers.redis = {
      enable = true;
      port = 6379;
    };
  };

  # Flask Web Server
  systemd.services.skarabaeus = {
    description = "Skarabäus Web Interface";
    serviceConfig = {
      User = "${user_name}";
      WorkingDirectory = "/home/${user_name}/skarabaeus/apps/gui/";
      ExecStart = "/home/${user_name}/skarabaeus/apps/gui/.venv/bin/python -m flask run -h 0.0.0.0";
      Restart = "always";
    };
    wantedBy = [ "multi-user.target" ];
    after = [ "network.target" ];
    environment = {
      FLASK_CONFIG = "development"; # change me
      DATABASE_URL = "sqlite:////home/${user_name}/skarabaeus/apps/gui/app/app.sqlite3"; # move me to zfs, maybe backup
      RESTIC_DIR = "${restic_repo_path}";
      # PATH = "/home/leroy/skarabaeus/apps/gui/.venv/bin:$PATH";
    };
  };

  # rq
  systemd.services.rq_worker = {
    description = "rq";
    serviceConfig = {
      User = "${user_name}";
      WorkingDirectory = "/home/${user_name}/skarabaeus/apps/gui/";
      ExecStart = "/run/current-system/sw/bin/poetry run rq worker tasks --with-scheduler";
      Restart = "always";
    };
    wantedBy = [ "multi-user.target" ];
    after = [ "network.target" ];
  };

  # rqscheduler
  systemd.services.rqscheduler = {
    description = "rqscheduler";
    serviceConfig = {
      User = "${user_name}";
      WorkingDirectory = "/home/${user_name}/skarabaeus/apps/gui/";
      ExecStart = "/run/current-system/sw/bin/poetry run rqscheduler --host localhost --port 6379 --db 0";
      Restart = "always";
    };
    wantedBy = [ "multi-user.target" ];
    after = [ "network.target" ];
  };

  systemd.services.skarabaeus.enable = true;
  systemd.services.rq_worker.enable = true;
  systemd.services.rqscheduler.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 8384 5000 ]; # 5000 for flask developemnt
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?

}
