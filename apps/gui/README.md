# Skarabäus Web Front-End
#### web interface for managing backups hosted on the skarabäus hardware

## Development

Create, upgrade and downgrade the database
```shell
flask db init
flask db migrate -m "new table"
flask db upgrade
flask db downgrade
flask db show
flask db heads  # returns the hash of the current migration
flask db history -r current:  # returns the last taken step and it's description
```

Apply all migrations
```shell
flask db upgrade
```

Running `rq`
```shell
rq worker tasks --with-scheduler 
```

Running `rqscheduler`
```shell
rqscheduler --host localhost --port 6379 --db 0
```

Running tests
```shell
pytest
```

Running ruff
```shell
poetry shell
python -m ruff check --exclude=migrations/*
python -m ruff format --exclude=migrations/*
```

Running the application
```shell
flask run
```