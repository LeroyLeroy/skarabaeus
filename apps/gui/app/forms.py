from flask_wtf import FlaskForm
from flask_wtf.file import FileRequired, MultipleFileField
from sqlalchemy import select
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, Email, ValidationError

from app.models import db, User


class RegistrationForm(FlaskForm):  # type: ignore[misc]
    """Web form to register new user"""

    email = StringField("Email", validators=[DataRequired(), Email()])
    password = StringField("Password", validators=[DataRequired()])
    confirm_note = BooleanField("Password has been written down", validators=[DataRequired()])
    confirm_no_recovery = BooleanField("The password can not be restored", validators=[DataRequired()])
    submit = SubmitField("Register")

    def validate_email(self, email) -> None:
        """emails should be unique"""
        user = db.session.scalar(select(User).where(User.email == email.data))
        if user is not None:
            raise ValidationError("Please use a different email address.")


class LoginForm(FlaskForm):  # type: ignore[misc]
    """Web form to login an existing user"""

    email = StringField("Email", validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[DataRequired()])
    totp = StringField("TOTP")
    remember_me = BooleanField("Remember Me")
    submit = SubmitField("Sign In")


class TOTPForm(FlaskForm):
    """setup TOTP"""

    setup_code = StringField("TOTP", validators=[DataRequired()])
    submit = SubmitField("Confirm")


class UploadForm(FlaskForm):  # type: ignore[misc]
    """Web form to files"""

    backup_files = MultipleFileField(validators=[FileRequired()])
    submit = SubmitField("Upload")


class AddSyncDeviceForm(FlaskForm):  # type: ignore[misc]
    """Web form to add a new syncthing device"""

    device_id = StringField("Device ID", validators=[DataRequired()])
    device_name = StringField("Device Name")
    submit = SubmitField("Just add a new device")


class AddSyncDeviceToFolderForm(FlaskForm):  # type: ignore[misc]
    """Web form to add a new syncthing device to a folder"""

    device_id = StringField("Device ID", validators=[DataRequired()])
    submit = SubmitField("Add device to the default folder")


class SyncFolderBackupForm(FlaskForm):
    """Web form to reboot the device"""
    submit = SubmitField("Backup Sync Folder")


class RebootDeviceForm(FlaskForm):
    """Web form to reboot the device"""
    submit = SubmitField("Reboot")


class ShutdownDeviceForm(FlaskForm):
    """Web form to reboot the device"""
    submit = SubmitField("Shutdown")
