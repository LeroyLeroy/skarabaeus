import os

from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
parent_of_basedir = os.path.abspath(os.path.join(basedir, ".."))


class Config:
    """config for the current flask app"""

    load_dotenv()

    SECRET_KEY: str = os.environ.get("SECRET_KEY") or "you-will-never-guess"
    LOG_DIR: str = os.path.join(parent_of_basedir, "logs")
    SQLALCHEMY_DATABASE_URI: str = os.environ.get(
        "DATABASE_URL"
    ) or "sqlite:///" + os.path.join(basedir, "app.sqlite3")
    REDIS_URL: str = os.environ.get("REDIS_URL") or "redis://"
    RESTIC_UPLOAD_DIR: str = os.environ.get("RESTIC_UPLOAD_DIR") or os.path.join(
        ".", "restic_upload"
    )
    RESTIC_RESTORE_DIR: str = os.environ.get("RESTIC_RESTORE_DIR") or os.path.join(
        ".", "restic_restore"
    )
    RESTIC_DIR: str = os.environ.get("RESTIC_DIR") or os.path.join(
        parent_of_basedir, "restic"
    )
    RESTIC_PASSWORD_FILE: str = os.environ.get("RESTIC_PASSWORD_FILE") or os.path.join(
        parent_of_basedir, "password.txt"
    )
    RESTIC_REPO_ID: str = "0"
    RESTIC_BIN_PATH: str = (
        os.environ.get("RESTIC_BIN_PATH") or "/home/leroy/.nix-profile/bin/restic"
    )
    API_URL_WITH_PORT: str = (
        os.environ.get("API_URL_WITH_PORT") or "http://localhost:8000"
    )
    SYNCTHING_API_KEY: str = os.environ.get("SYNCTHING_API_KEY") or "skarabaeus_testing"
    SYNCTHING_DEFAULT_FOLDER_PATH: str = os.environ.get("SYNCTHING_DEFAULT_FOLDER_PATH") or os.path.join(
        parent_of_basedir, "syncthing"
    )
    SYNCTHING_DEFAULT_FOLDER_ID: str = os.environ.get("SYNCTHING_DEFAULT_FOLDER_ID") or "testing"
