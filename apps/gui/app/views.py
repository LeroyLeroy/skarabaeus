import os

from flask import (
    Blueprint,
    render_template,
    redirect,
    url_for,
    flash,
    current_app,
    send_from_directory,
)
from flask_login import login_required, login_user, logout_user, current_user
from xkcdpass import xkcd_password as xp
from sqlalchemy import select
from werkzeug import Response
from werkzeug.utils import secure_filename
import pyotp

from app import ResticWorker, DeviceInfo, SyncthingWorker
from app.models import db, Task
from app.forms import (LoginForm, RegistrationForm, UploadForm, AddSyncDeviceForm,
                       RebootDeviceForm, ShutdownDeviceForm, AddSyncDeviceToFolderForm, TOTPForm, SyncFolderBackupForm)
from app.models import User

main = Blueprint("main", __name__, template_folder="templates")
auth = Blueprint("auth", __name__, template_folder="templates")


@login_required  # type: ignore[misc]
@main.route("/backup", methods=["GET", "POST"])
def backup():
    """uploading files to backup"""
    if not current_user.is_authenticated:
        return redirect(url_for("main.index"))

    upload_form = UploadForm()
    if upload_form.validate_on_submit():
        list_of_files = []
        for f in upload_form.backup_files.data:
            filename = secure_filename(f.filename)
            file_with_path = os.path.join(
                current_app.config["RESTIC_UPLOAD_DIR"], filename
            )
            f.save(file_with_path)
            list_of_files.append(file_with_path)

        if current_user.get_task_in_progress("backup_files"):
            flash("An backup_files task is currently in progress")
            return render_template("index.html", title="Home")

        try:
            task = current_user.launch_task("backup_files", list_of_files)
            db.session.commit()
        except Exception as exc:
            raise RuntimeError from exc
        flash(f"Backup started, task-id: {task.id}")

    sync_backup_form = SyncFolderBackupForm()
    if sync_backup_form.validate_on_submit():
        if current_user.get_task_in_progress("backup_files"):
            flash("An backup_files task is currently in progress")
            return render_template("index.html", title="Home")

        default_folder = current_app.config["SYNCTHING_DEFAULT_FOLDER_PATH"]
        list_of_files = [os.path.join(dirpath, f) for (dirpath, dirnames, filenames) in os.walk(default_folder) for f in filenames]
        try:
            task = current_user.launch_task("backup_files", list_of_files)
            db.session.commit()
        except Exception as exc:
            raise RuntimeError from exc
        flash(f"Backup started, task-id: {task.id}")

    return render_template(
        "backup.html",
        title="Upload",
        upload_form=upload_form,
        sync_backup_form=sync_backup_form
    )


@login_required  # type: ignore[misc]
@main.get("/restore")
def restore():
    """listing all files in a snapshot"""
    if not current_user.is_authenticated:
        return redirect(url_for("main.index"))

    restic_worker = ResticWorker(logger=current_app.logger)
    # todo: try me b
    response = restic_worker.files_in_snapshot()
    if len(response["errors"]):
        for error in response["errors"]:
            flash(error)

    return render_template("restore.html", title="Restore Files", files=response["files"])


@login_required  # type: ignore[misc]
@main.get("/restore/<snapshot_id>/<path>/<file_name>")
def restore_file(snapshot_id: str, path: str, file_name: str):
    """listing all files in a snapshot"""
    if not current_user.is_authenticated:
        return redirect(url_for("main.index"))

    restic_worker = ResticWorker(logger=current_app.logger)
    # todo: try me b
    was_restored = restic_worker.restore(snapshot_id, path, file_name)
    if was_restored:
        flash("The file has been restored!")
        # the reference to the parent dir is needed as the current working dir is /app
        restore_dir = "../" + current_app.config["RESTIC_RESTORE_DIR"]
        return send_from_directory(restore_dir, file_name, as_attachment=True)
    else:
        flash(
            f"Something went wrong! "
            f'The restoring process for the file "{file_name}" failed!'
        )

    return redirect(url_for("main.restore"))


@login_required  # type: ignore[misc]
@main.get("/delete/<snapshot_id>/<path>/<file_name>")
def delete_file(snapshot_id: str, path: str, file_name: str):
    """listing all files in a snapshot"""
    if not current_user.is_authenticated:
        return redirect(url_for("main.index"))

    restic_worker = ResticWorker(logger=current_app.logger)
    # todo: try me b
    was_deleted = restic_worker.remove_file(snapshot_id, path, file_name)
    if not was_deleted:
        flash(
            f"Something went wrong! "
            f'The deleting process for the file "{file_name}" failed!'
        )
    else:
        flash("The file has been deleted!")

    return redirect(url_for("main.restore"))


@login_required  # type: ignore[misc]
@main.route("/device", methods=["GET", "POST"])
def device() -> Response | str:
    if not current_user.is_authenticated:
        return redirect(url_for("main.index"))

    reboot_form = RebootDeviceForm()
    if reboot_form.validate_on_submit():
        device_info: DeviceInfo = current_app.extensions["device_info"]
        device_info.reboot(1)
        flash("Device will reboot shortly")

    shutdown_form = ShutdownDeviceForm()
    if shutdown_form.validate_on_submit():
        device_info: DeviceInfo = current_app.extensions["device_info"]
        device_info.shutdown(1)
        flash("Device will shutdown shortly")

    scheduler = current_app.extensions["scheduler"]
    list_of_job_instances = scheduler.get_jobs()
    return render_template(
        "device.html",
        title="Device",
        tasks=list_of_job_instances,
        reboot_form=reboot_form,
        shutdown_form=shutdown_form
    )


@login_required  # type: ignore[misc]
@main.route("/sync", methods=["GET", "POST"])
def sync() -> Response | str:
    # todo: this always submits both forms, pls fix
    if not current_user.is_authenticated:
        return redirect(url_for("main.index"))

    syncthing_worker: SyncthingWorker = current_app.extensions["syncthing"]

    add_device_form = AddSyncDeviceForm()
    if add_device_form.validate_on_submit():
        if not add_device_form.device_name.data:
            device_name = "unknown name"
        else:
            device_name = add_device_form.device_name.data
        result = syncthing_worker.add_device(add_device_form.device_id.data, device_name)
        if not result:
            current_app.logger.error("failed to add syncthing device")
            flash("failed to add syncthing device")
            return redirect(url_for("main.sync"))
        else:
            flash("Added SyncThing device")

    device_to_folder_form = AddSyncDeviceToFolderForm()
    if device_to_folder_form.validate_on_submit():
        result = syncthing_worker.share_folder_with_device(
            syncthing_worker.default_folder_id,
            device_to_folder_form.device_id.data
        )
        if not result:
            current_app.logger.error("failed to add device to folder")
            flash("failed to add device to folder")
            return redirect(url_for("main.sync"))
        else:
            flash("Added device to default folder")

    files = syncthing_worker.get_files_in_folder()
    return render_template(
        "sync.html",
        title="Sync",
        files=files,
        add_device_form=add_device_form,
        device_to_folder_form=device_to_folder_form
    )


@login_required  # type: ignore[misc]
@main.get("/account")
def account() -> Response | str:
    if not current_user.is_authenticated:
        return redirect(url_for("main.index"))

    return render_template("account.html", title="Account")


@login_required  # type: ignore[misc]
@main.get("/result/<task_id>")
def task_result(task_id: str) -> Response | str | dict[str, str | int | bool]:
    """get the current status of a rq task"""
    if not current_user.is_authenticated:
        return redirect(url_for("main.index"))

    task = db.session.get(Task, task_id)
    return {
        "name": task.name,
        "id": task.id,
        "complete": task.complete,
        "user_id": task.user_id,
    }


@main.get("/index")
@main.get("/")
@login_required  # type: ignore[misc]
def index() -> str:
    """render the main template"""
    return render_template("index.html", title="Home")


@auth.route("/register", methods=["GET", "POST"])
def register() -> str | Response:
    """
    render the register template or
    check the submitted form and create a new user
    """
    if current_user.is_authenticated:
        return redirect(url_for("main.index"))

    form = RegistrationForm()
    # user submitted the form
    if form.validate_on_submit():
        user = db.session.scalar(select(User).where(User.email == "placeholder"))
        if not user.valid_password(form.password.data):
            flash("Invalid password")
            return redirect(url_for("auth.register"))

        user.email = form.email.data
        db.session.commit()
        # todo: tell the API and confirm the password was saved
        flash("Congratulations, you are now a registered user!")
        return redirect(url_for("auth.login"))

    # create new user
    word_file = xp.locate_wordfile("ger-anlx")  # german word list
    random_words = xp.generate_wordlist(
        wordfile=word_file,
        min_length=5,
        max_length=6,
        valid_chars="[a-z]"  # no umlaut
    )
    password = xp.generate_xkcdpassword(random_words, numwords=7, delimiter="-")
    user = db.session.scalar(select(User).where(User.email == "placeholder"))
    if user is None:
        user = User(email="placeholder")
    user.set_password(password)
    db.session.add(user)
    db.session.commit()
    return render_template("register.html", title="Register", form=form, password=password)


@auth.route("/create_totp", methods=["GET", "POST"])
def create_totp() -> str | Response:
    user = db.session.scalar(select(User).where(User.email == current_user.email))
    if user.totp_confirmed:
        flash("Error! Already set up TOTP!")
        return redirect(url_for("main.index"))
    
    form = TOTPForm()
    if form.validate_on_submit():
        totp = pyotp.TOTP(user.totp_secret)
        if not totp.verify(form.setup_code.data):
            flash("Not valid! Try again!")
            return redirect(url_for("auth.create_totp"))
        else:
            user.totp_confirmed = True
            db.session.commit()
            flash("Successfully set up TOTP!")
            return redirect(url_for("main.index"))

    user.totp_secret = pyotp.random_base32()
    db.session.commit()
    qr_code_text = pyotp.totp.TOTP(
        user.totp_secret
    ).provisioning_uri(name=user.email, issuer_name='Skarabäus')
    return render_template("totp.html", title="TOTP", form=form, qr_code_text=qr_code_text)


@auth.route("/login", methods=["GET", "POST"])
def login() -> str | Response:
    """
    render the login template or login the user and redirect to the main page
    """
    if current_user.is_authenticated:
        return redirect(url_for("main.index"))
    form = LoginForm()
    if form.validate_on_submit():
        user = db.session.scalar(select(User).where(User.email == form.email.data))
        # check if user exists, the password is valid, and if TOTP is set up, if it is valid
        if (user is None
                or not user.valid_password(form.password.data)
                or (user.totp_confirmed and not user.valid_totp(form.totp.data))):
            flash("Invalid username or password")
            return redirect(url_for("auth.login"))
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for("main.index"))
    return render_template("login.html", title="Sign In", form=form)


@auth.get("/logout")
@login_required  # type: ignore[misc]
def logout() -> Response:
    """
    remove session vars, cookies,
    and redirect the user to the login page
    """
    logout_user()
    return redirect(url_for("main.index"))
