import time
from rq import get_current_job

from app import create_app, ResticWorker
from app import db
from app.models import Task

app = create_app()
app.app_context().push()


def set_task_progress(progress: int) -> None:
    """update the task to 'complete'"""
    job = get_current_job()
    if job:
        job.meta["progress"] = progress
        job.save_meta()
        task = db.session.get(Task, job.get_id())
        if progress >= 100:
            task.complete = True
        db.session.commit()


def example(seconds: int) -> None:
    job = get_current_job()
    print("Starting task")
    for i in range(seconds):
        job.meta["progress"] = 100.0 * i / seconds
        job.save_meta()
        print(i)
        time.sleep(1)
    job.meta["progress"] = 100
    job.save_meta()
    set_task_progress(100)
    print("Task completed")


def backup_files(paths: list[str]) -> None:
    """task to backup files"""
    restic_worker = ResticWorker(logger=app.logger)
    # todo: try me b
    restic_worker.backup(paths)
    set_task_progress(100)


def snapshot_files(snapshot_id) -> list[dict]:
    """task to list files of a snapshot"""
    restic_worker = ResticWorker(logger=app.logger)
    # todo: try me b
    output = restic_worker.list_snapshot(snapshot_id)
    set_task_progress(100)
    return output
