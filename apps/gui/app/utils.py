import json
import os
import datetime
import re
import sys
from logging import Logger
import subprocess
from zoneinfo import ZoneInfo

from flask import current_app, Flask
from pyncthing import Syncthing
from requests import HTTPError
from sqlalchemy import select
import requests
import restic
from restic import stats, snapshots, restore, backup, cat, check, find, rewrite, forget
from restic.errors import ResticFailedError

from app.models import db, User


def run_subprocess(logger: Logger, command: str) -> str:
    """
    command is a str
    returns the output or an empty string
    """
    result = None
    try:
        result = subprocess.run(command, capture_output=True, text=True, shell=True)
    except Exception as e:
        logger.error(f'subprocess Exception for command: "{command}"')
        logger.error(e)

    if result.returncode != 0:
        logger.error(f'subprocess "{command}" did not return 0')
        logger.error(result.stderr)
        return ""

    return result.stdout


def format_bytes(size) -> str:
    # 2**10 = 1024
    power = 2 ** 10
    n = 0
    power_labels = {0: '', 1: 'kilo', 2: 'mega', 3: 'giga', 4: 'tera'}
    while size > power:
        size /= power
        n += 1
    return f"{round(size, 2)} {power_labels[n]}bytes"


class DeviceInfo:
    """device specific information"""

    def __init__(
            self,
            logger,
            api_url_with_port,
            config_path: str = "/etc/nixos/configuration.nix",
    ) -> None:
        self.logger: Logger = logger
        self.api_url_with_port = api_url_with_port
        self.os_version: str = self.get_os_version()
        self.config_path = config_path
        self.needed_packages = self.get_needed_packages()
        self.storage_capacity = self.get_zfs_pool_capacity()
        self.storage_allocated = self.get_zfs_pool_allocated()
        self.storage_free = self.get_zfs_pool_free()

    def get_os_version(self) -> str:
        """return the nixos version"""
        return run_subprocess(self.logger, "nixos-version")

    def validate_os_config_syntax(self) -> bool:
        """checks the nixos config syntax"""
        check_config = f"nix-instantiate {self.config_path} --parse-only"
        output = run_subprocess(self.logger, check_config)
        if not output:
            self.logger.error(output)
            return False
        return True

    def is_zfs_pool_degraded(self) -> bool:
        """notice if the pool is degraded"""
        zfs_status = "zpool status -v bigdata"
        status_output = run_subprocess(self.logger, zfs_status)
        if "state: DEGRADED" in status_output:
            return True
        else:
            return False

    def get_zfs_pool_capacity(self):
        """"""
        zfs_capacity = "zpool list -H -o size"
        capacity = run_subprocess(self.logger, zfs_capacity)
        return capacity

    def get_zfs_pool_allocated(self):
        """"""
        zfs_allocated = "zpool list -H -o allocated"
        allocated = run_subprocess(self.logger, zfs_allocated)
        return allocated

    def get_zfs_pool_free(self):
        """"""
        zfs_free = "zpool list -H -o free"
        free = run_subprocess(self.logger, zfs_free)
        return free

    def reboot(self, minutes: int):
        reboot = f"shutdown -r +{minutes}"
        output = os.system(reboot)
        if "Reboot scheduled for" in output:
            self.logger.info(f"reboot scheduled in {minutes} minutes")
        else:
            self.logger.error("scheduling reboot failed: ")
            self.logger.error(output)

    def shutdown(self, minutes: int):
        shutdown = f"shutdown -h +{minutes}"
        output = os.system(shutdown)
        if "Shutdown scheduled for" in output:
            self.logger.info(f"shutdown scheduled in {minutes} minutes")
        else:
            self.logger.error("scheduling shutdown failed: ")
            self.logger.error(output)

    def get_needed_packages(self) -> list[dict]:
        """get the package list from the backend API"""
        try:
            response = requests.get(
                f"{self.api_url_with_port}/needed_packages", timeout=5
            )
        except requests.exceptions.ConnectionError as e:
            self.logger.error(
                f"requests failed connecting to the API at: "
                f"{self.api_url_with_port}"
            )
            self.logger.error(e)
            return []

        if response.status_code == 200:
            return response.json()

        self.logger.error("failed to retrieve the currently needed package list")
        self.logger.error(response)
        return []

    def validate_software_with_version(self) -> list[dict]:
        """check the version of the installed packages"""
        for package in self.needed_packages:
            check_package = (
                f'nix-store -q --references '
                f'/run/current-system/sw'
                f' | grep {package["name"]}'
            )
            installed_package = run_subprocess(self.logger, check_package)
            if not installed_package:
                self.logger.error(
                    f'package "{package["name"]}", '
                    f'does not seem to be installed, '
                    f'continue to next package'
                )
                continue

            check_package_version = (
                f'nix-store -q --references '
                f'/nix/store/{package["hash"]}'
                f'-{package["name"]}'
                f'-{package["version"]}'
            )
            matching_package_version = run_subprocess(
                self.logger, check_package_version
            )
            if not matching_package_version:
                self.logger.error(
                    f'package "{package["name"]}",'
                    f' does not have the required version'
                )
            else:
                package["validated"] = True

        return self.needed_packages

    def register_with_backend(self, app: Flask, email: str, password: str) -> bool:
        """register the current user with the backend API"""
        with app.app_context():
            user = db.session.scalar(select(User).where(User.email == email))
            if not user:
                self.logger.error("register with backend failed with no user")
                return False
            backend_endpoint = "/user/add"
            headers = {
                "accept": "application/json",
            }
            params = {
                "email": user.email,
                "password": password,
            }
            response = requests.post(
                f"{self.api_url_with_port}{backend_endpoint}",
                params=params,
                headers=headers,
                timeout=5,
            )
            if response.status_code == 200:
                return True
            if response.status_code == 406:
                return True
            return False

    def set_token_from_backend(self, app: Flask, email: str, password: str) -> bool:
        """request the user token from the backend and save it"""
        with app.app_context():
            user = db.session.scalar(select(User).where(User.email == email))
            backend_endpoint = "/user/token"
            headers = {
                "accept": "application/json",
            }
            data = {
                "username": user.email,
                "password": password,
            }
            response = requests.post(
                f"{self.api_url_with_port}{backend_endpoint}",
                data=data,
                headers=headers,
                timeout=5,
            )
            if response.status_code == 200:
                user.token = response.json()["access_token"]
                db.session.commit()
                return True

            return False

    def confirm_register_with_backend(self, app: Flask, email: str) -> bool:
        """request the backend API to confirm the user"""
        with app.app_context():
            user: User = db.session.scalar(select(User).where(User.email == email))
            headers = {
                "accept": "application/json",
                "Authorization": f"Bearer {user.token}",
            }
            params = {
                "email": user.email,
            }
            response = requests.post(
                f"{self.api_url_with_port}/user/confirm",
                params=params,
                headers=headers,
                timeout=5,
            )
            return True if response.status_code == 200 else False


class SyncthingWorker:
    """syncthing"""

    def __init__(self, logger: Logger) -> None:
        self.logger: Logger = logger
        self.rest = Syncthing("http://localhost:8384/rest")
        self.rest.set_api_key(current_app.config.get('SYNCTHING_API_KEY'))
        self.default_folder_path = current_app.config.get("SYNCTHING_DEFAULT_FOLDER_PATH")
        self.default_folder_id = current_app.config.get("SYNCTHING_DEFAULT_FOLDER_ID")

        if not self.is_syncthing_usable():
            self.logger.error("syncthing dir has previously been created, but is not usable. Aborting!")
            sys.exit()

    def is_syncthing_usable(self) -> bool:
        # assume the dir on the filesystem and folder_id in SyncThing, exists
        try:
            assert self.default_folder_id is not None and self.default_folder_id is not ""
            assert self.default_folder_path is not None and self.default_folder_path is not ""
        except AssertionError as e:
            self.logger.error("syncthing setup error: folder_id not set or default path does not exist")
            self.logger.error(f"syncthing setup error: folder_id is: '{self.default_folder_id}', "
                              f"default path is: '{self.default_folder_path}'")
            self.logger.error(e)
            return False
        try:
            status = self.rest.system.status()
        except requests.exceptions.ConnectionError as e:
            self.logger.error("failed to connect to SyncThing REST-API, host not up")
            self.logger.error(e)
            return False
        except requests.exceptions.HTTPError as e:
            self.logger.error("failed to connect to SyncThing REST-API, token not set")
            self.logger.error(e)
            return False
        try:
            folder_status = self.rest.db.status(self.default_folder_id)
        except requests.exceptions.HTTPError as e:
            self.logger.error("syncthing default folder is not usable")
            self.logger.error(e)
            return False
        if status["uptime"] and status["myID"] and not folder_status["errors"]:
            return True
        return False

    def restart_syncthing(self):
        self.rest.system.restart()
        self.logger.info("restarted syncthing")

    def add_device(self, client_id: str, device_name: str) -> bool:
        pattern = r'([0-9A-Z]{7}-?)'
        match = re.search(pattern, client_id)
        if not match:
            self.logger.error("client_id is not a valid syncthing id")
            self.logger.error(f"client_id was: '{client_id}'")
            return False

        new_device = {
            'deviceID': client_id,
            'name': device_name,
            'addresses': ['dynamic'],
            'compression': 'metadata',
            'certName': '',
            'introducer': False,
            'skipIntroductionRemovals': False,
            'introducedBy': '',
            'paused': False,
            'allowedNetworks': [],
            'autoAcceptFolders': False,
            'maxSendKbps': 0,
            'maxRecvKbps': 0,
            'ignoredFolders': [],
            'maxRequestKiB': 0,
            'untrusted': False,
            'remoteGUIPort': 0,
            'numConnections': 0
        }
        try:
            response = self.rest.config.devices().post(new_device)
        except HTTPError as e:
            self.logger.error("client_id is not a valid syncthing id, syncthing said so")
            self.logger.error(e)
            return False
        if response.status_code == 200:
            return True
        else:
            self.logger.error("failed to add new device, more infos: ")
            self.logger.error(response.json())
            return False

    def add_folder(self, label: str, path: str, device_ids: list[str]) -> bool:
        new_folder = {
            'id': label,
            'label': label,
            'filesystemType': 'basic',
            'path': path,
            'type': 'sendreceive',
            'devices': [{'deviceID': device, 'introducedBy': '', 'encryptionPassword': ''} for device in device_ids],
            'rescanIntervalS': 3600,
            'fsWatcherEnabled': True,
            'fsWatcherDelayS': 10,
            'ignorePerms': False,
            'autoNormalize': True,
            'minDiskFree': {
                'value': 1,
                'unit': '%'
            },
            'versioning': {
                'type': '',
                'params': {},
                'cleanupIntervalS': 3600,
                'fsPath': '',
                'fsType': 'basic'
            },
            'copiers': 0,
            'pullerMaxPendingKiB': 0,
            'hashers': 0,
            'order': 'random',
            'ignoreDelete': False,
            'scanProgressIntervalS': 0,
            'pullerPauseS': 0,
            'maxConflicts': 10,
            'disableSparseFiles': False,
            'disableTempIndexes': False,
            'paused': False,
            'weakHashThresholdPct': 25,
            'markerName': '.stfolder',
            'copyOwnershipFromParent': False,
            'modTimeWindowS': 0,
            'maxConcurrentWrites': 2,
            'disableFsync': False,
            'blockPullOrder': 'standard',
            'copyRangeMethod': 'standard',
            'caseSensitiveFS': False,
            'junctionsAsDirs': False,
            'syncOwnership': False,
            'sendOwnership': False,
            'syncXattrs': False,
            'sendXattrs': False,
            'xattrFilter': {
                'entries': [],
                'maxSingleEntrySize': 1024,
                'maxTotalSize': 4096
            }
        }
        try:
            response = self.rest.config.folders().post(new_folder)
        except requests.exceptions.HTTPError as e:
            self.logger.error("failed to add new folder, more infos: ")
            self.logger.error(e)
            return False
        if response.status_code == 200:
            return True
        else:
            self.logger.error("failed to add new folder, more infos: ")
            self.logger.error(response.json())
            return False

    def share_folder_with_device(self, folder_id, device_id) -> bool:
        response = self.rest.config.folders(folder_id).get()
        # todo: this check does not work, on error response is info about syncthing instance
        if not len(response):
            self.logger.error(f"failed to get folder by id: '{folder_id}', more infos: ")
            self.logger.error(response)
            return False

        response['devices'].append({'deviceID': device_id, 'introducedBy': '', 'encryptionPassword': ''})
        result = self.rest.config.folders(folder_id).put(response)
        if result.status_code == 200:
            return True
        else:
            self.logger.error("failed to add new folder, more infos: ")
            self.logger.error(result.json())
            return False

    def get_files_in_folder(self):
        return self.rest.db.browse(folder=self.default_folder_id, levels=None, prefix='')


class ResticWorker:
    """restic information and interactions"""

    def __init__(self, logger: Logger) -> None:
        self.logger: Logger = logger
        self.repo_id: str = current_app.config["RESTIC_REPO_ID"]
        self.repo_dir: str = current_app.config.get("RESTIC_DIR")
        self.password_file: str = current_app.config.get("RESTIC_PASSWORD_FILE")
        self.upload_dir: str = current_app.config.get("RESTIC_UPLOAD_DIR")
        self.restore_dir: str = current_app.config.get("RESTIC_RESTORE_DIR")
        self.latest_snapshot_id: str = ""
        self.backup_file_count: int = 0
        self.restore_file_count: int = 0
        self.last_backup_date: datetime = None
        self.last_restore_date: datetime = None
        restic.binary_path = current_app.config.get("RESTIC_BIN_PATH")
        restic.password_file = self.password_file
        restic.repository = self.repo_dir

        if not self.is_repo_usable():
            self.logger.error(
                "restic dir has previously been created, "
                "but is not usable. Aborting!"
            )
            sys.exit()

    def repo_stats(self) -> dict:
        """simple stats about the repository"""
        current_stats = {}
        try:
            current_stats = stats(mode="raw-data")
        except ResticFailedError as e:
            self.logger.error(
                "restic stats could not be retrieved, "
                "indicating the repository could not be found"
                "exception below"
            )
            self.logger.error(e)
        return current_stats

    def check_repo(self) -> bool:
        """check the integrity of the repository"""
        output = check(read_data=True)
        if not output:
            self.logger.error("repo self check failed")
            return False
        self.logger.info("checking the repo returned: ")
        self.logger.info(output)
        if output and "no errors were found" in output:
            return True
        else:
            return False

    def is_repo_usable(self) -> bool:
        # todo: check return value, and raise errors if needed
        """simple check if the repo is as expected"""
        return bool(len(self.repo_stats())) and self.check_repo()

    def repo_config(self) -> dict:
        """get the repo configuration and return it"""
        output = cat.config()
        if not output:
            self.logger.error("repo config is not valid, see below")
            self.logger.error(output)
            return output

        # todo: check repo_id is the current one
        # self.repo_id = output["id"]
        return output

    def backup(self, paths: list[str]) -> bool:
        """creates a backup of a given input list of files"""
        try:
            output = backup(paths=paths)
        except ResticFailedError as e:
            self.logger.error("restic backup failed")
            self.logger.error(e)
            return False

        if not output["snapshot_id"]:
            self.logger.error("restic backup failed, no new snapshot_id, output is:")
            self.logger.error(output)
            return False

        self.latest_snapshot_id = output["snapshot_id"]
        self.backup_file_count += output["files_new"]
        current_date = datetime.datetime.now()
        date_with_timezone = datetime.datetime(
            current_date.year,
            current_date.month,
            current_date.day,
            tzinfo=ZoneInfo("Europe/Zurich"),
        )
        self.last_backup_date = date_with_timezone
        self.logger.info("created backup of the following: ")
        for item in paths:
            self.logger.info(f'file: "{item}"')
        return True

    def restore(self, snapshot_id: str, path: str, include: str) -> bool:
        """restore files from a snapshot"""
        snapshot_path = ":restic_upload" if "restic_upload" in path else ""
        try:
            # snapshot id with the path avoids restoring the dir itself
            output = restore(
                snapshot_id + snapshot_path, include, target_dir=self.restore_dir
            )
        except ResticFailedError as e:
            self.logger.error("restic restore failed")
            self.logger.error(e)
            return False

        # the output is weird, this converts it to a dict
        output = json.loads(output.split("\n")[0])
        if not output["files_restored"]:
            self.logger.error("restic restore failed, no files_restored, " "output is:")
            self.logger.error(output)
            return False

        self.restore_file_count += output["files_restored"]
        current_date = datetime.datetime.now()
        date_with_timezone = datetime.datetime(
            current_date.year,
            current_date.month,
            current_date.day,
            tzinfo=ZoneInfo("Europe/Zurich"),
        )
        self.last_restore_date = date_with_timezone
        return True

    def find_file(self, file_name: str) -> list[dict]:
        """returns an empty list if none is found"""
        output = find(file_name)
        if not output:
            self.logger.error(f"no search result for file: " f'"{file_name}"')
        return output

    def remove_file(self, snapshot_id: str, path: str, file_name: str) -> bool:
        """returns an empty list if none is found"""
        base_path = "/restic_upload/" + file_name \
            if "restic_upload" in path \
            else current_app.config.get("SYNCTHING_DEFAULT_FOLDER_PATH") + "/" + file_name
        try:
            output = rewrite(exclude=[base_path], forget=True, snapshot_id=snapshot_id)
            if "removed old snapshot" not in output:
                self.logger.error("rewriting the snapshot failed")
                self.logger.error(output)
                # todo: maybe abort

            forget(prune=True)
        except ResticFailedError as e:
            self.logger.error("restic rewrite failed")
            self.logger.error(e)
            return False
        return True

    def list_snapshot(self, snapshot_id: str) -> list[dict]:
        """returns an empty list if none is found"""
        output: list[dict] = snapshots(snapshot_id=snapshot_id)
        if not output:
            self.logger.error(
                f"searched for snapshot with id: " f'"{snapshot_id}" without result!'
            )
        return output

    def files_in_snapshot(self, snapshot_id: str = "latest") -> dict:
        files = []
        try:
            output: list[dict] = snapshots(snapshot_id)
        except ResticFailedError as e:
            self.logger.error(f"failed to get snapshot by id: {snapshot_id}")
            self.logger.error(e)
            return {"files": files, "errors": [f"failed to get snapshot by id: '{snapshot_id}'", "see error log"]}

        for snapshot in output:
            for path in snapshot["paths"]:
                file = find(path.rsplit("/", 1)[1])
                if file and file[0]["hits"]:
                    current_file = file[0]["matches"][0]
                    current_file["file_name"] = current_file["path"].rsplit("/", 1)[1]
                    current_file["snapshot_id"] = file[0]["snapshot"]
                    current_file["permissions"] = current_file["permissions"]
                    current_file["backup_datetime"] = current_file["mtime"]
                    current_file["base_path"] = "restic_upload" if "restic_upload" in current_file["path"] else "syncthing"
                    files.append(current_file)
        sorted_by_date = sorted(files, key=lambda my_file: my_file["backup_datetime"])
        return {"files": sorted_by_date, "errors": []}
