from datetime import datetime
from typing import Any

import pyotp
import redis
import rq
import sqlalchemy as sa
from flask import current_app
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func, ForeignKey, ScalarResult
from sqlalchemy.orm import relationship, mapped_column, Mapped, WriteOnlyMapped
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin

db = SQLAlchemy()


class User(UserMixin, db.Model):  # type: ignore[name-defined, misc]
    """local user"""

    __tablename__ = "users"
    id: Mapped[int] = mapped_column(primary_key=True, nullable=False)
    email: Mapped[str] = mapped_column(
        sa.String(120), index=True, unique=True, nullable=False
    )
    password_hash: Mapped[str] = mapped_column(sa.String(256))
    password_confirmed: Mapped[bool] = mapped_column(default=False)
    totp_secret: Mapped[str] = mapped_column(sa.String(256), nullable=False, default="")
    totp_confirmed: Mapped[bool] = mapped_column(default=False)
    token: Mapped[str] = mapped_column(sa.String(256), nullable=False, default="")
    created_at: Mapped[datetime] = mapped_column(
        sa.DateTime(timezone=True), default=func.now()
    )

    devices: WriteOnlyMapped["Device"] = relationship(back_populates="owner")
    tasks: WriteOnlyMapped["Task"] = relationship(back_populates="user")

    def __repr__(self) -> str:
        return f"<User {self.email}>"

    def set_password(self, password: str) -> None:
        """store the user password only as a secure hash"""
        self.password_hash = generate_password_hash(password)

    def valid_password(self, password: str) -> bool:
        """compare the user supplied input with the stored one"""
        return check_password_hash(self.password_hash, password)

    def valid_totp(self, code: str) -> bool:
        """check the one time password"""
        totp = pyotp.TOTP(self.totp_secret)
        return totp.verify(code)

    def launch_task(self, name: str, *args, **kwargs) -> Any:
        """start a new task"""
        rq_job = current_app.task_queue.enqueue(f"app.tasks.{name}", *args, **kwargs)
        task = Task(id=rq_job.get_id(), name=name, user=self)
        db.session.add(task)
        return task

    def get_tasks_in_progress(self) -> ScalarResult:
        """get all unfinished tasks"""
        query = self.tasks.select().where(Task.complete is False)
        return db.session.scalars(query)

    def get_task_in_progress(self, name) -> ScalarResult:
        """get task by name (function name)"""
        query = self.tasks.select().where(Task.name == name, Task.complete is False)
        return db.session.scalar(query)


class Device(db.Model):  # type: ignore[name-defined,misc]
    """Information about the current device"""

    __tablename__ = "devices"
    id: Mapped[int] = mapped_column(primary_key=True)
    device_version: Mapped[str] = mapped_column(default="0.0.9", nullable=False)
    os_version: Mapped[str] = mapped_column(default="0.0.9", nullable=False)
    owner_id: Mapped[int] = mapped_column(ForeignKey("users.id"), nullable=False)
    created_at: Mapped[datetime] = mapped_column(
        sa.DateTime(timezone=True), default=func.now(), nullable=False
    )
    last_online_at: Mapped[datetime] = mapped_column(
        sa.DateTime(timezone=True), nullable=True
    )
    first_online_at: Mapped[datetime] = mapped_column(
        sa.DateTime(timezone=True), nullable=True
    )
    owner: Mapped["User"] = relationship(back_populates="devices")

    def __repr__(self) -> str:
        return f"<Device {self.id}> for user {self.owner_id}>"


class Task(db.Model):
    """rq tasks"""

    id: Mapped[str] = mapped_column(sa.String(36), primary_key=True)
    name: Mapped[str] = mapped_column(sa.String(128), index=True)
    user_id: Mapped[int] = mapped_column(sa.ForeignKey(User.id))
    complete: Mapped[bool] = mapped_column(default=False)

    user: Mapped[User] = relationship(back_populates="tasks")

    def __repr__(self) -> str:
        return f"<Task {self.id}>"

    def get_rq_job(self):
        """get job by id"""
        try:
            rq_job = rq.job.Job.fetch(self.id, connection=current_app.redis)
        except (redis.exceptions.RedisError, rq.exceptions.NoSuchJobError):
            return None
        return rq_job

    def get_progress(self):
        """task progress"""
        job = self.get_rq_job()
        return job.meta.get("progress", 0) if job is not None else 100


class Changelog(db.Model):  # type: ignore[name-defined,misc]
    """to keep track of every interaction a changelog table is used"""

    __tablename__ = "changelogs"
    id: Mapped[int] = mapped_column(primary_key=True)
    table: Mapped[str] = mapped_column(index=True, nullable=False)
    changelog: Mapped[str] = mapped_column(index=True, nullable=False)
    created_at: Mapped[datetime] = mapped_column(
        sa.DateTime(timezone=True), default=func.now(), nullable=False
    )

    def __repr__(self) -> str:
        return f"<Changelog {self.id}>"
