import logging
import os
import sys
from datetime import datetime
from logging.handlers import RotatingFileHandler

import redis
import rq
from flask import Flask
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_qrcode import QRcode
from redis import Redis
from rq import Queue
from rq_scheduler import Scheduler

from app.models import db, User
from app.utils import ResticWorker, DeviceInfo, SyncthingWorker, format_bytes
from app.views import main, auth
from app.config import Config

basedir = os.path.abspath(os.path.dirname(__file__))
login_manager = LoginManager()
login_manager.login_view = "auth.login"
migrate = Migrate()


def create_app() -> Flask:
    """init new flask app and bootstrap"""
    app = Flask(__name__)
    app.config.from_object(Config)

    if not app.debug:
        log_dir: str = app.config.get("LOG_DIR")
        if not os.path.exists(log_dir):
            os.mkdir(log_dir)
        file_handler = RotatingFileHandler(
            os.path.join(log_dir, "skarabäus_gui.log"), maxBytes=51200, backupCount=100
        )
        file_handler.setFormatter(
            logging.Formatter(
                "%(asctime)s %(levelname)s: " "%(message)s [in %(pathname)s:%(lineno)d]"
            )
        )
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)

    app.config.from_prefixed_env()
    login_manager.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)
    init_restic_app(app, app.logger)
    init_syncthing_app(app, app.logger)
    init_device_info(app, app.logger)
    QRcode(app)

    # tasks
    default_queue = "tasks"
    app.redis = Redis.from_url(app.config["REDIS_URL"])
    app.task_queue = rq.Queue(default_queue, connection=app.redis)

    queue = Queue(default_queue, connection=app.redis)
    scheduler = Scheduler(queue=queue, connection=queue.connection)
    app.extensions["scheduler"] = scheduler

    start_periodic_tasks(app)

    # jinja_env
    app.jinja_env.globals.update(
        format_bytes=format_bytes,
    )

    # blueprint
    app.register_blueprint(main)
    app.register_blueprint(auth)
    app.logger.setLevel(logging.INFO)
    app.logger.info("Skarabäus GUI startup")
    return app


def init_restic_app(app: Flask, logger: logging.Logger) -> ResticWorker:
    """create a new restic repository and check its status"""
    with app.app_context():
        restic_worker = ResticWorker(logger=logger)

    app.extensions["restic"] = restic_worker
    app.logger.info("created restic app")
    return restic_worker


def init_syncthing_app(app: Flask, logger: logging.Logger) -> SyncthingWorker:
    """create a new restic repository and check its status"""
    with app.app_context():
        syncthing_worker = SyncthingWorker(logger=logger)

    app.extensions["syncthing"] = syncthing_worker
    app.logger.info("created syncthing app")
    return syncthing_worker


def init_device_info(app: Flask, logger: logging.Logger) -> DeviceInfo:
    """create device_info app"""
    with app.app_context():
        device_info = DeviceInfo(
            logger=logger,
            api_url_with_port=app.config.get("API_URL_WITH_PORT"),
            config_path="/etc/nixos/configuration.nix",
        )

    app.extensions["device_info"] = device_info
    app.logger.info("created device_info")
    return device_info


def start_periodic_tasks(app: Flask):
    device_info = app.extensions["device_info"]
    restic_worker = app.extensions["restic"]
    scheduler = app.extensions["scheduler"]
    # every 5min check if the zpool is in a degraded state
    try:
        scheduler.schedule(
            scheduled_time=datetime.utcnow(),
            func=device_info.is_zfs_pool_degraded,
            args=[],
            kwargs={},
            interval=60 * 5,
            repeat=None,
            ttl=172800,  # 2 days
            meta={}
        )
        # every 5min check the integrity of the restic repo
        scheduler.schedule(
            scheduled_time=datetime.utcnow(),
            func=restic_worker.check_repo,
            args=[],
            kwargs={},
            interval=60 * 5,
            repeat=None,
            ttl=172800,  # 2 days
            meta={}
        )
    except redis.exceptions.ConnectionError as e:
        app.logger.error("failed to connect to redis, aborting")
        app.logger.error(e)
        sys.exit()

    app.logger.info("started periodic tasks")


@login_manager.user_loader  # type: ignore[misc]
def load_user(user_id: str) -> object | None:
    """get the current user from the db"""
    return db.session.get(User, int(user_id))
