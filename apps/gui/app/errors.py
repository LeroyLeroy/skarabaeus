from flask import render_template

from app.models import db
from app.views import main


@main.errorhandler(404)  # type: ignore[type-var]
def not_found_error() -> tuple[str, int]:
    """
    renders the 404 template
    """
    return render_template("404.html"), 404


@main.errorhandler(500)
def internal_error(error: str) -> tuple[str, int]:
    """
    renders the 500 template
    """
    db.session.rollback()
    return render_template("500.html", error=error), 500
