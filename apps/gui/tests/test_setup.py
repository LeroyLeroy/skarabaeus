from app import create_app
from app.utils import run_subprocess

app = create_app()


def test_flask_migration():
    output = run_subprocess(app.logger, "poetry shell; flask db show")
    assert "Revision ID: 2dbe1b25216b" in output
