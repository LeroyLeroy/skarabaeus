import os

from app import create_app, SyncthingWorker

app = create_app()


def test_syncthing():
    syncthing_worker: SyncthingWorker = app.extensions["syncthing"]
    result = syncthing_worker.rest.system.status()
    # breakpoint()
    assert result['myID'] == os.getenv('SYNCTHING_ID')
    assert syncthing_worker.add_device('6F2JY4E-G7YLZEZ-4J5ZRKQ-DL5WBK7-PRLX2DE-JHUXMGZ-VE6LUO2-YSIBWAE', 'test')
    assert syncthing_worker.add_folder('testing', '/home/leroy/testing', ['GMKMC7W-VRZLFRU-DKH2Q4Q-6EBCGKZ-73PPUNT-NTCS2J3-AAT4TMI-3UN5TAY'])
    assert syncthing_worker.share_folder_with_device('testing', 'GMKMC7W-VRZLFRU-DKH2Q4Q-6EBCGKZ-73PPUNT-NTCS2J3-AAT4TMI-3UN5TAY')
    devices = syncthing_worker.rest.config.devices().get()
    assert len(devices)
