import pytest

from app import create_app

app = create_app()


@pytest.fixture
def client():
    app.config.update({"TESTING": True})

    with app.test_client() as client:
        yield client


def test_register_page(client):
    response = client.get("/register")
    assert response.status_code == 200
    assert (
        b'<input id="submit" name="submit" type="submit" value="Register">'
        in response.data
    )


def test_login_page(client):
    response = client.get("/login")
    assert response.status_code == 200
    assert b'New User? <a href="/register">Click to Register!</a>' in response.data
