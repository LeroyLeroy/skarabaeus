from app import create_app, DeviceInfo

app = create_app()


def test_needed_packages():
    device_info: DeviceInfo = app.extensions["device_info"]
    assert device_info.needed_packages == [
        {
            "name": "restic",
            "version": "0.16.4",
            "hash": "6n0hfv7klm0zkidc6nd1h5k1mq1cikzi",
            "validated": False,
        }
    ]
    assert "23.11" in device_info.os_version
    assert device_info.validate_os_config_syntax()
    assert device_info.validate_software_with_version() == [
        {
            "name": "restic",
            "version": "0.16.4",
            "hash": "6n0hfv7klm0zkidc6nd1h5k1mq1cikzi",
            "validated": True,
        }
    ]


def test_register_with_backend():
    device_info: DeviceInfo = app.extensions["device_info"]
    email = "l@b.ch"
    password = "1"
    assert device_info.register_with_backend(app=app, email=email, password=password)
    assert device_info.set_token_from_backend(app=app, email=email, password=password)
    assert device_info.confirm_register_with_backend(app=app, email=email)


def test_reboot():
    pass
    # device_info: DeviceInfo = app.extensions["device_info"]
    # device_info.reboot(4)
