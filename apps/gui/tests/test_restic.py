import shutil

from app import create_app, ResticWorker

app = create_app()


def test_restic_components():
    restic_worker: ResticWorker = app.extensions["restic"]
    assert len(restic_worker.repo_stats())
    assert restic_worker.check_repo()
    assert restic_worker.is_repo_usable()


def test_restic():
    test_file = "poetry.lock"
    test_file_destination = "./restic_upload"
    shutil.copy(test_file, test_file_destination)
    test_file_path = test_file_destination + "/" + test_file
    restic_worker: ResticWorker = app.extensions["restic"]
    assert restic_worker.backup([test_file_path])
    snapshot = restic_worker.list_snapshot("latest")
    assert (
        snapshot[0]["paths"][0]
        == "/home/leroy/repos/skarabaeus/apps/gui/restic_upload/poetry.lock"
        or "/home/leroy/Documents/private_repos/skarabaeus/apps/gui/restic_upload/poetry.lock"
    )
    snapshot_list = restic_worker.find_file(test_file)
    assert snapshot_list[0]["matches"][0]["path"] == "/restic_upload/poetry.lock"
    assert restic_worker.restore(snapshot_id="latest", include=test_file)
