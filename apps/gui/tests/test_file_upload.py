import os
import shutil
from time import sleep

import pytest
from flask_login import current_user
from sqlalchemy import select
from werkzeug.datastructures import FileStorage

from app import create_app, ResticWorker, User, db

app = create_app()


@pytest.fixture(scope="session")
def client():
    app.config.update(
        {"TESTING": True, "LOGIN_DISABLED": True, "WTF_CSRF_ENABLED": False}
    )

    with app.test_client() as client:
        yield client


def test_file_upload(client):
    tmp_test_dir = "./tests/tmp/"
    os.mkdir(tmp_test_dir)

    num_files = 2
    # num_files = 1000
    file_size = 1024 * 1024  # 1MB
    # file_size = 1024 * 30720  # 30MB

    files_to_upload = []

    for i in range(num_files):
        filename = f"random_file_{i}.leroy"
        file_with_path = tmp_test_dir + filename

        with open(file_with_path, "wb") as f:
            f.write(os.urandom(file_size))

        files_to_upload.append(
            FileStorage(
                stream=open(file_with_path, "rb"),
                filename=filename,
                content_type="text/plain",
            )
        )

    with app.app_context():
        test_email = 'l@b.ch'
        user = db.session.scalar(select(User).where(User.email == test_email))
        if user is None:
            new_user = User(email=test_email)
            new_user.set_password('1')
            db.session.add(new_user)
            db.session.commit()

    login_response = client.post(
        "/login", data={"email": "l@b.ch", "password": "1"}, follow_redirects=True
    )

    assert login_response.status_code == 200
    assert current_user.is_authenticated

    response = client.post(
        "/backup",
        data={"backup_files": files_to_upload},
        content_type="multipart/form-data",
        follow_redirects=False,
    )
    assert response.status_code == 200
    assert b"Backup started, task-id: " in response.data

    shutil.rmtree(tmp_test_dir)

    sleep(60)

    restic_worker: ResticWorker = app.extensions["restic"]
    assert restic_worker.restore(snapshot_id="latest", include="random_file_1.leroy")
