import pytest

from app import create_app

app = create_app()


@pytest.fixture
def client():
    app.config.update({"TESTING": True})

    with app.test_client() as client:
        yield client


def test_register_form(client):
    response = client.post(
        "/register",
        data={
            "email": "example@example.com",
            "password": "password",
            "password2": "password",
        },
    )
    assert response.status_code == 200
